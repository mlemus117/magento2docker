#  Magento 2 Docker Development

### NGINX 1.12 + PHP 7.1-fpm + PerconaDB 5.7 + N98 Magerun 2 + XDebug + Redis + RabbitMQ + Mailhog

### MacOS / Windows / Linux Requirements

#### Setup Prerequisites

**Linux (Docker, Docker-compose and Docker-sync)**

Install Docker-CE: Use this [official Docker install](https://docs.docker.com/engine/installation/linux/docker-ce/ubuntu/).

Install Docker-Compose: Use this tutorial [Docker-Compose Install](https://www.digitalocean.com/community/tutorials/how-to-install-docker-compose-on-ubuntu-16-04)

Install Docker-sync [official Docker-sync install](http://docker-sync.io/).

```
  gem install docker-sync

```
####Using Unison Strategy

The Ubuntu package for unison doesn't come with `unison-fsmonitor`, as such, we would need to build from source.
```
sudo apt-get install build-essential ocaml
wget https://github.com/bcpierce00/unison/archive/2.48.4.tar.gz
tar xvf 2.48.4.tar.gz
cd unison-2.48.4
make UISTYLE=text
sudo cp src/unison /usr/local/bin/unison
sudo cp src/unison-fsmonitor /usr/local/bin/unison-fsmonitor
and that should be enough to get you up and running using unison.
```

**MacOS (Docker, Docker-compose and Docker-sync)**

Install Docker for Mac [offcial Docker install](https://docs.docker.com/docker-for-mac/install/).

Docker for Mac comes with the latest Docker, Docker-Engine, and Docker-Compose versions.

Install Docker-sync [official Docker-sync install](http://docker-sync.io/).
```
  sudo gem install docker-sync
```

**Windows (Docker, Docker-compose and Docker-sync)**

Install Docker for Windows [offcial Docker install](https://docs.docker.com/docker-for-windows/install/).

Docker for Windows comes with the latest Docker, Docker-Engine, and Docker-Compose versions.

Install Docker-sync [official Docker-sync install](http://docker-sync.io/).
```
  gem install docker-sync
```

#### **Clone this repository to your machine and then switch to the branch matching your OS**

#### Checklist

Before moving on to the installation make sure the following services are not running:

1. Vagrant
2. XAMP/XEMP
3. MAMP/MEMP
4. WAMP/WEMP


### Installation

After pulling this repository you can build the environment for Magento 2 and deploy it locally. The Magento 2 repository is separated from here for pipeline deployment and other build concerns for production.

Here's how to use this setup:

1. Install Docker as per instructions listed above
2. Download the Magento 2 repository and name the directory "src/":
* ```git clone https://USERNAME@bitbucket.org/mlemus117/ivg-magento2ee.git src```
3. Start Docker-Sync in order to sync the src/ directory between the host machine and Docker. This solves permission issues on the files between the host user (you) and the app user (app). In Linux there are issues with UID, fixes are coming.
* ```docker-sync start```
4. Now start the docker-compose file to build the containers (can take about 10 mins to build):
* ```docker-compose up -d```
5. For Linux users you need to change the ownership of the hidden npm folder that was generate after starting up docker
* ```sudo chown -R 1001:1001 .npm```
6. There's a bunch of helper scripts installed on the app container and also here in the repo to speed up development. Use the ./shell script to enter the container:
* ```./shell```
7. Inside the container now you can deploy Magento 2:
* ```deploy-magento2```
8. Magento 2 is complete and here are the services you can access:

| Service  | Address  | User  | Pass  | Description  |
|---|---|---|---|---|
| Magento 2   | 127.0.0.1   |   |   | The customer facing application   |
| Magento 2 Admin  | 127.0.0.1/admin  | magento2  | admin123  | Magento 2 administration page  |
| PhpMyAdmin  | 127.0.0.1:8080  |   |   | Database Administration Console  |
| MailHog  | 127.0.0.1:8025  |   |   | Email Catcher for testing/debugging  |
| RabbitMQ  | 127.0.0.1:15762  | rabbitmq  | rabbitmq  | Message Queue management interface  |


### Helper scripts (outside app)

A couple of scripts have been added so that every day tasks are a bit easier.

| Command | Description |
|---|---|
| `./magento $@` | Run the magento cli command specified (`./magento list` shows list of commands) |
| `./n98 $@` | Run the n98-magerun command (`./n98 list` shows list of commands) |
| `./shell` | Enter docker container as user app by default, or you can specify a user when running it. |
| `./fix-perms` | Corrects file system permission inside container |
| `./composer $@` | Runs composer command inside container (`./composer install` runs the install command) |
| `./grunt-init` | Installs Grunt and NodeJS dependencies |
| `./grunt` | Runs Grunts exec; less; deploy; watch by default or you can supply a command and it'll exec;  less; watch; the command |
| `./cli $@` | Runs suppled command as `root` inside container. BE CAREFUL |
| `./xdebug $@` | Only takes `enable` or `disable` to start Xdebug. You can get more information about Xdebug here: [Xdebug Home](https://xdebug.org/) |

### Helper scripts (inside app)

A couple of scripts have been added so that every day tasks are a bit easier. They have to be done from inside the app so you must enter it first via docker commands or through `./shell`

| Command | Description |
|---|---|
| `deploy-magento2` | Setups and install magento 2 with the existing source directory |
| `fix-permission` | chmod 777 folder and give `bin/magento` permission to execute. This is under progress for more granular permissions |
| `update-magento2` | Run update commands to check for changes and install/update/upgrade new files and deploy |
| `install-magento2 $@` | Downloads and installs Magento 2 as per the supplied version number. Folder src/ must exist and be empty to work. |

### Daily Use

#### Startup

When starting up the app you need to have Docker-sync running first so:
```docker-sync start```

Then startup the containers:
```docker-compose up -d```

#### Turn off

We need to stop the containers and the optionally you can stop docker-sync as well:
```
docker-compose stop

docker-sync stop

```

#### Go Nuclear

Need to redo your installation? Ok time to go nuclear:

```
docker-compose down #Stops containers and removes containers, networks, volumes, and images
docker-sync stop #Stop sync'ing folders
docker-sync clean #Remove volume created by docker-sync

```

Optionally if you run `docker system df` and it looks like docker is taking up too much memory from the kernel you can follow up with these commands:

```
docker image prune -a #delete all unused images
docker volume prune #delete all unused volumes
```
